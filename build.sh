#!/usr/bin/env bash
rm -rf public/*

hugo
REPO=${1:-git@codeberg.org:GuildAlpha/website}
DATE=$(date -u -Is)
git clone $REPO pages.git
rsync -av public/* pages.git/

cd pages.git &&
git checkout --orphan current &&
git add -A &&
git commit -m "Deployment at $DATE" &&
git branch -m pages &&
git push -f origin pages &&
cd .. &&
rm -rf pages.git
