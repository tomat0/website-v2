---
title: "Conclusion of the First Sprint"
date: 2023-09-18T23:21:17-05:00
draft: false
---

At the beginning of the summer, members of the [Social Coding Movement](https://discuss.coding.social/) started Guild Alpha as a way to alleviate the [lack of organization](https://share.tube/w/uHR55dAJmUUjF7haNwCYt6) within the free-software and Fediverse communities. In addition, we sought to provide a platform to developers and enthusiasts everywhere to help contribute to the same platforms they love using.

So, we started our first "sprint": a 10-week run to find a project we like, decide on a feature or extension that needs to be added, and get it done. The following is about what we did, and how you can help us out this upcoming season.

# What is a Guild?

Simply put, a "guild" is an informal collective of people across the internet who band together to stamp out various issues in FOSS projects, learn new skills, and have fun along the way. Guild Alpha is the first of these, we hope to create a model others can replicate.

Whereas traditional software development would add features by hiring developers in a formal structure, we look to embrace the non-commercial aspects of free-software and take matters into our own hands.

Anyone can join, and that includes you! Just stop by [our Matrix chat](https://matrix.to/#/%23guild-alpha-lounge:matrix.org) and we'll get you up to speed. Even if you don't plan to participate, you can still hang out, chat, and keep an eye on progress.

# What did we set out to do?

The project we decided on for Summer 2023 was to add instance migration to Bookwyrm, a federated "social reading" site (think Goodreads, but federated/FOSS).

Currently, when you sign up on a Bookwyrm instance, your books, reviews, quotes, and all other information are tied to that instance. If that instance were to go down or if you were to want to move instances, you'd have to start over again. This lack of mobility can end up locking you into one instance. Creating a system by which you can export and import your data would solve this.

# What did we accomplish?

Thanks to the efforts of our contributors, we were able to successfully implement the core functionality for exporting and importing account data. There remains some work to be done, such as admin controls and notifications, but we were able to bring our progress to a point where we could package it up and return to the Bookwyrm maintainers to continue work on.

We asked one of the Bookwyrm maintainers, hughrun, for a few words:

> Working with the Guild Alpha team was a little confusing at first, as I was contributing as a BookWyrm contributor rather than as a Guild Alpha member and had not heard of the concept before BookWyrm was chosen as the project to work on.
>
> It took a little while to iron out initial setup, as the Guild Alpha group didn't want to use the GitHub + Docker infrastructure used by BookWyrm (which is understandable, but did mean it took a week or so to get things set up before any "real" work could begin). The Guild Alpha team are primarily based in Europe and I am based in Australia so real-time communication was difficult, however this sometimes has its advantages as I could work "overnight".
>
> Overall the experience was very rewarding for me personally as I haven't worked with a group on a specific feature before. The Guild members also clearly have a lot of coding skill and were both able to complete the parts I wasn't sure how to implement, and direct the initial implementation in a cleaner, more understandable direction.
>
> Account migration is a key feature for BookWyrm, the need for which has been increasingly obvious. Implementation of account migration will make the overall BookWyrm network more robust as it will allow for users to move between instances when the one they originally chose becomes overwhelmed or administrators are no longer able to maintain the server.

### Credits

A special thanks goes out to the following members for their help with the project this season:

-   CSDUMMI

-   dannymate

-   hughrun

-   Tomat0

-   Ryuno-Ki

-   circlebuilder
